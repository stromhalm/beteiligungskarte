<?php

//Import Excel-Library
require_once dirname(__FILE__) . '/../bundles/PHPExcel/Classes/PHPExcel.php';

//Import Backend-Files
function __autoload($className) {
	$path = 'classes/' . $className . '.php';
	
	if (file_exists($path)) {
		require_once $path;
	} else {
		return false;
	}
}

$manager = new GeneralManager();

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Niedersächsischer Landesjugendring")
							 ->setLastModifiedBy("Niedersächsischer Landesjugendring")
							 ->setTitle("Statement Export")
							 ->setSubject("Statement Export")
							 ->setDescription("Die Statements der neXTmap")
							 ->setKeywords("LJR neXTmap")
							 ->setCategory("Export file");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Statement')
            ->setCellValue('B1', 'Längengrad')
            ->setCellValue('C1', 'Breitengrad')
            ->setCellValue('D1', 'Ort')
            ->setCellValue('E1', 'Kategorie');
            
$statements = $manager->getAllStatements();

foreach ($statements as $column => $statement) {
	
	$objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A' . ($column + 2), $statement->title)
	            ->setCellValue('B' . ($column + 2), $statement->longitude)
	            ->setCellValue('C' . ($column + 2), $statement->latitude)
	            ->setCellValue('D' . ($column + 2), $statement->location)
	            ->setCellValue('E' . ($column + 2), $statement->category);
}

//Style für die Titel-Zeile
$styleArray = array(
	'font' => array(
	'bold' => true
	)
);

for ($col = 'A'; $col != 'F'; $col++) {
	//column width
	$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
	
	//style fpr main row
	$objPHPExcel->getActiveSheet()->getStyle($col . '1')->applyFromArray($styleArray);
}

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Statement Export');

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="neXTmap-Export.xls"');
header('Cache-Control: max-age=0');

// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
