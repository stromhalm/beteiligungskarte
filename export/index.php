<!DOCTYPE html>
<html ng-app="UserInterface1">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="neXTmap">
		<meta name="author" content="Jumax">

		<title>neXTmap</title>

		<!-- CSS -->
		<link href="../bundles/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../bundles/css/style.css?ver=3" />
		
		<script type='text/javascript' src='https://www.google.com/jsapi'></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=visualization"></script>
		<script>
		
		var map, pointarray, heatmap;
		
		var statements = [
		  
		  <?php
		  	
		  	//Import Backend-Files
		  	function __autoload($className) {
		  		$path = '../backend/classes/' . $className . '.php';
		  		
		  		if (file_exists($path)) {
		  			require_once $path;
		  		} else {
		  			return false;
		  		}
		  	}
		  	
		  	$manager = new GeneralManager();
		  	$statements = $manager->getAllStatements();
		  	
		  	foreach ($statements as $key => $statement) {
		  		if ($key != 0) { echo ", "; }
		  		echo "new google.maps.LatLng(" . $statement->latitude . ", " . $statement->longitude . ")";
		  	}
		  
		  ?>
		];
		
		function initialize() {
		  var mapOptions = {
		    zoom: 6,
		    center: new google.maps.LatLng(51.2681, 10.3657)
		  };
		
		  map = new google.maps.Map(document.getElementById('map-canvas'),
		      mapOptions);
		
		  var pointArray = new google.maps.MVCArray(statements);
		
		  heatmap = new google.maps.visualization.HeatmapLayer({
		    data: pointArray
		  });
			
			heatmap.set('radius', 20);
		  heatmap.setMap(map);
		}
		
		function changeGradient() {
		  var gradient = [
		    'rgba(0, 255, 255, 0)',
		    'rgba(0, 255, 255, 1)',
		    'rgba(0, 191, 255, 1)',
		    'rgba(0, 127, 255, 1)',
		    'rgba(0, 63, 255, 1)',
		    'rgba(0, 0, 255, 1)',
		    'rgba(0, 0, 223, 1)',
		    'rgba(0, 0, 191, 1)',
		    'rgba(0, 0, 159, 1)',
		    'rgba(0, 0, 127, 1)',
		    'rgba(63, 0, 91, 1)',
		    'rgba(127, 0, 63, 1)',
		    'rgba(191, 0, 31, 1)',
		    'rgba(255, 0, 0, 1)'
		  ]
		  heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
		}
		
		google.maps.event.addDomListener(window, 'load', initialize);
		
		</script>

	</head>
	<body ng-controller="PhotoBooth">
	
		<!-- Static navbar -->
		<div class="navbar navbar-default navbar-static-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="../" ng-click="reset()"><span class="glyphicon glyphicon-map-marker"></span>neXTmap</a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="hidden-xs hidden-sm"><a target="_blank" href="http://www.ljr.de"><img class="sponsor ljr" src="../bundles/img/logos/ljr.png" alt="LJR" /></a></li>
						<li class="hidden-xs hidden-sm"><a target="_blank" href="http://nlm.de"><img class="sponsor nlm" src="../bundles/img/logos/4_nlm.png" alt="NLM" /></a></li>
						<li class="hidden-xs hidden-sm"><a class="impressum" target="_blank" href="http://www.ljr.de/Impressum.impressum.0.html">Impressum</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Container for the pages -->
		<div class="container">

			<!-- Page 1: The Webcam -->
			<div class="row">
				<div class="col-md-4 start-center">
					<div class="box download-box">
					
						<h1>Excel-Export</h1>
						
						<a class="btn btn-primary btn-lg" href="../backend/export.php">
							<span class="glyphicon glyphicon-download"></span><br />
							Download
						</a><br />
						Alle Statements als Excel-Datei herunterladen
							
					</div>
					
					<div class="box">
					
						<h1>Statistik</h1>
						
						Anzahl Statements: <b><?php echo count($statements); ?></b>
							
					</div>
					
				</div>
				
				<div class="col-md-8 start-center">
					<div class="box">
						<h1>Heatmap</h1>
						<div id="map-canvas" style="width: auto; height: 600px;"></div>
					</div>
				</div>
				
			</div>
			
			<div class="row footer">
				<div class="col-md-6">© <script>document.write(new Date().getFullYear())</script> entwickelt von <a target="_blank" title="jumax.net" href="http://jumax.net">Jumax</a>, Grafik <a title="a04.de" target="_blank" href="http://a04.de">a04.de</a>, Icons <a title="icons8.com" target="_blank" href="http://icons8.com/download-huge-windows8-set/">icons8.com</a></div>
			</div>
			
		</div> <!-- /container -->

		<!-- Bootstrap core JavaScript -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="../bundles/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>