
var mapApp = angular.module("beteiligungskarte-map", ["google-maps"]);

mapApp.controller('mapCtrl', function ($scope, $http, $filter) {
	
	/**
	 * The settings and options for the map
	 */
	$scope.map = {
		center: {
			latitude: 51.1642292,
			longitude: 10.4541194
		},
		zoom: 6,
		options: {
			streetViewControl: false,
			maxZoom: 9,
			minZoom: 6,
		}
	};

	/**
	 * The markers on the map
	 * 
	 * TODO: Rename to statements
	 */
	$scope.markers = [];

	/**
	 * The single window that displays
	 * all the statements on the map.
	 */
	$scope.window = {
		show: false,
		coords: {
			latitude: 50.5,
			longitude: 23
		},
		options: {
			pixelOffset: new google.maps.Size(0, -36)
		}
	};

	/**
	 * Sidebar options
	 */
	$scope.sidebar = {
		cssClass: '',
		addCtrlClass: '',
		toggleOverview: null, // defined later
		toggleAdditionalControls: null // defined later
	};

	/**
	 * Refresh options
	 * 
	 * The refreshFunction calls the function responsible
	 * for the new statements from the server.
	 */
	$scope.refresh = {
		counter: 0,
		interval: 20000, // 20 sec
		timer: null,
		refreshFunction: null // defined later
	};

	/**
	 * Cycle options
	 * 
	 * The cycleFunction activates a statement at random.
	 */
	$scope.cycle = {
		counter: 0,
		interval: 8000, // 3 sec
		timer: null,
		cycleFunction: null, // defined later
		changeInterval: null // defined later
	};

	/**
	 * Stores the currently active statement so
	 * it can be deactivated.
	 */
	$scope.activeStatement = null;

	/**
	 * Function to center the map. Currently unused.
	 */
	$scope.setCenter = function(lat, lng) {
		$scope.map.center.latitude = lat;
		$scope.map.center.longitude = lng;
	};

	/**
	 * Toggles the sidebar between normal and overview state by
	 * adding/reoving the css class 'overview'.
	 */
	$scope.sidebar.toggleOverview = function() {

		if($scope.sidebar.cssClass != 'overview'){
			$scope.sidebar.cssClass = 'overview';
		} else {
			$scope.sidebar.cssClass = '';
		}
	}

	$scope.sidebar.toggleAdditionalControls = function() {

		if($scope.sidebar.addCtrlClass != 'open'){
			$scope.sidebar.addCtrlClass = 'open';
		} else {
			$scope.sidebar.addCtrlClass = '';
		}
	}

	/**
	 * Adds new statements from the server or all
	 * statements (only at start).
	 */
	$scope.getAllStatements = function() {
		
		//Completly replace the statements (all statements are loaded each time)
		var startAt = '';
//		if($scope.markers.length <= 0) {
//			var startAt = '';
//		} else {
//			startAt = $scope.markers[0].id
//		}
		var data = {
			'action' : 'get-statements',
			'startAt': startAt
		}

		$http({
			method: 'POST', 
			url: '../backend/index.php', 
			data: data, 
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}, 
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
		}}).
		success(function(data, status, headers, config) {
			
			// If there are new statements load them
			if($scope.markers.length != data.statements.length) {
			
				//Completly replace the statements (all statements are loaded each time)
				$scope.markers = [];
			
				for(index in data.statements) {
					
					//Completely replace the statements (all statements are loaded each time)
					$scope.markers.unshift(data.statements[index]);
					
					//Add coords in seperate attribute
					data.statements[index].coords = {
						latitude: data.statements[index].latitude,
						longitude: data.statements[index].longitude
					};
					data.statements[index].show = null;
				}
			}
		}).
		error(function(data, status, headers, config) {
			console.log(data);
		});
	}

	/**
	 * Marker event click
	 */
	$scope.markerEvents = {
		click: function(gMarker, eventName, model) {

			model.show = true;
			// Apply is needed
			$scope.$apply(function () {
				$scope.activateStatement(model.id);
			});
		}
	};

	/**
	 * Activates a statement
	 *
	 * Activation means that the statement gets a 
	 * window on the map and highlighted in the sidebar.
	 */
	$scope.activateStatement = function(id) {

		var statement = $scope.getStatement(id);

		if(statement === undefined) {
			console.error("Cannot activate statement. Statement is undefined");
			return;
		}
		
		if($scope.activeStatement != null) {
			$scope.activeStatement.isSelected = "";
		}
		
		//Deactivate all other statements
		angular.forEach($scope.markers, function(thisStatement, nr) {
			thisStatement.show = null;
		});
		
		$scope.activeStatement = statement;
		statement.show = true;

		$scope.activeStatement.isSelected = "active";

		$scope.window.coords.latitude = statement.latitude;
		$scope.window.coords.longitude = statement.longitude;

		$scope.window.show = true;
	}

	/**
	 * Deletes a statement
	 */
	$scope.deleteStatement = function(statement) {

		var data = {
			'action' : 'delete-statement',
			'id': statement.id
		}

		$http({
			method: 'POST', 
			url: '../backend/index.php', 
			data: data, 
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}, 
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
		}}).
		success(function(data, status, headers, config) {

			// Remove one element from the array
			var index = $scope.markers.indexOf(statement);
			$scope.markers.splice(index, 1);
		}).
		error(function(data, status, headers, config) {
			console.log(data);
		});
	}

	/**
	 * Returns the statement with the given id
	 * This function searches the whole statements array.
	 */
	$scope.getStatement = function(id) {

		var result = $filter('filter')($scope.markers, function(value, index) {

			if(value != null) {
				return value.id == id;
			} else {
				return false;
			}
		});

		if (result.length > 0){
			return result[0];
		} else {
			return null;
		}
	}

	/**
	 * Refreshes the statements periodically.
	 * Options are in the array refresh.
	 */
	$scope.refresh.refreshFunction = function() {

		$scope.refresh.counter++;
		$scope.getAllStatements();

		setTimeout($scope.refresh.refreshFunction, $scope.refresh.interval);
	};
	$scope.refresh.refreshFunction();

	/**
	 * Cycles the statements periodically.
	 * Options are in the array cycle.
	 */
	$scope.cycle.cycleFunction = function() {

		$scope.cycle.counter++;

		var length = $scope.markers.length;
		if (length > 0) {

			$scope.$apply(function () {
				$scope.activateStatement( $scope.markers[randomInt(0,length-1)].id );
			});
		}

		setTimeout($scope.cycle.cycleFunction, $scope.cycle.interval);
	};
	$scope.cycle.cycleFunction();

	/**
	 * Called when the interval is changed via
	 * slider or input.
	 */
	$scope.cycle.changeInterval = function() {

		clearTimeout($scope.cycle.cycleFunction);
	};
	
	$scope.allStatementsOptions = {
		'pixelOffset': new google.maps.Size(0, -36)
	};

});

/**
 * Retunds a random integer between (including) min and max.
 */
function randomInt(min,max) {
	return Math.floor(Math.random()*(max-min+1)+min);
}