Hej!

Deine Stimme z&auml;hlt! Nur wer was sagt, wird geh&ouml;rt! Wir geben deine Botschaften an die Politik weiter und machen zusammen die Welt besser!

Du findest dein Statement online unter folgender URL: 
[link]

Dort kannst du auch sehen, was sich andere f&uuml;r ihren Ort w&uuml;nschen.

Vielen Dank f&uuml;r deinen Eintrag auf der neXTmap!

&hearts;
Dein Team
Landesjugendring Niedersachsen e.V.


Landesjugendring Niedersachsen e.V. | Zei&szlig;stra&szlig;e 13 | 30519 Hannover | www.ljr.de | info@ljr.de
Inhaltlich Verantwortlicher gem&auml;&szlig; &sect; 6 MDStV: Bj&ouml;rn Bertram | Gesch&auml;ftsf&uuml;hrer | Vertretungsberechtigter Vorstand: Jens Risse (Vorstandssprecher) | Susanne Martin (Schatzmeisterin), Katrin M&uuml;ller, Hannah Gundert | Vereinsregister: Amtsgericht Hannover | Nr.: 4479 | StNr.: 25/207/22953