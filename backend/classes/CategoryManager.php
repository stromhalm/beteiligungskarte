<?php 

/**
 * Has useful functions for categories.
 */
class CategoryManager {

	private $categoryFile = 'categories.txt';
	
	/**
	 * Returns all categories as an array.
	 *
	 * @return array 	The array of Categories
	 */
	public function getAllCategories() {

		$categories = array();
		$categoriesTXT = file_get_contents("../" . $this->categoryFile);

		$categories = explode(PHP_EOL, $categoriesTXT);
		
		return $categories;
	}
	
	/**
	 * Returns all categories as json string.
	 *
	 * @return string or boolean 	Categories as JSON
	 */
	public function getAllCategoriesAsJson() {

		$categories = $this->getAllCategories();
		if ( empty($categories) ) {
			return false;
		}
		return json_encode($categories);
	}
}

?>