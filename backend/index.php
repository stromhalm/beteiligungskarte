<?php

function __autoload($className) {
	$path = 'classes/' . $className . '.php';

	if (file_exists($path)) {
		require_once $path;
	} else {
		return false;
	}
}

header('Content-Type: application/json; charset=utf-8');

$manager = new GeneralManager();

$action = $_POST['action'];

switch ( $action ) {

	case 'add-statement':

		echo $manager->processFormData(); //Return the id of the new statement
		break;

	case 'delete-statement':

		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$manager->deleteStatement($id);
		break;

	case 'get-statements':

		if( isset($_POST['startAt']) && !empty($_POST['startAt']) ) {

			echo $manager->getStatementsAsJson($_POST['startAt']);
		} else {

			echo $manager->getStatementsAsJson();
		}

		break;

	case 'get-statement':

			if( isset($_POST['statementId']) && !empty($_POST['statementId']) ) {

				$statement = $manager->getStatementAsJson($_POST['statementId']);

				if ($statement) {
					echo $statement;
				} else {
					echo 'Not found';
				}
			}

			break;

	case 'send-email':

			if( isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['link']) && !empty($_POST['link']) ) {

				$manager->sendMail($_POST['email'], $_POST['link']);
				echo 'OK';
			}

			break;

	case 'get-categories':

		$catManager = new CategoryManager();
		echo $catManager->getAllCategoriesAsJson();
		exit;

		break;
}
?>
