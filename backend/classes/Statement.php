<?php

/**
 * Represents a Statement with an id (The directory name in which the statement
 * files are contained), a title, and two coordinates (latitude and longitude).
 */
class Statement {

	public $id;
	public $title;
	public $latitude;
	public $longitude;
	public $category;
	public $location;
	public $hasImage;

	function __construct($id, $title, $latitude, $longitude, $category, $location, $hasImage){
		$this->id = $id;
		$this->title = $title;
		$this->latitude = $latitude;
		$this->longitude = $longitude;
		$this->category = $category;
		$this->location = $location;
		$this->hasImage = $hasImage;
	}

	public function getId(){
		return $this->id;
	}
}

?>