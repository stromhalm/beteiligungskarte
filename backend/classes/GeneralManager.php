<?php 

/**
 * The "Main" class that manages everything that has to
 * do with the statements like creating, deleting and adding them.
 */
class GeneralManager {

	private $defaultDataDirectoryPath = '/backend/data';
	private $defaultDataDirectoryName = 'data';
	private $defaultDataFileName = 'statement.serialized.txt';
	private $defaultImageName = 'image.jpg';
	private $emailFrom = 'neXTmap@ljr.de';
	private $emailSubject = 'neXTmap';
	
	/**
	 * Returns all statements as an array of statement objects.
	 *
	 * @return array 	The array of Statement Objects
	 */
	public function getAllStatements() {

		$filenames = array();
		$statements = array();

		$directory = $this->getProjectDir() . $this->defaultDataDirectoryPath;
		$filenames = array_diff(scandir($directory), array('..', '.', '.DS_Store', 'thumbs.db'));

		foreach($filenames as $filename) {
			$statements[] = $this->getStatement($filename);
		}
		
		return $statements;
	}
	
	/**
	 * Returns a specific statement as a json string.
	 *
	 * @param  string   The statement id
	 * @return string or boolean 	The statement as json, false if not found
	 */
	public function getStatementAsJson($id) {

		$statement = $this->getStatement($id);
		if (!$statement) {
			return false;
		}
		return json_encode($statement);
	}

	/*
	 * Returns the latest statements up to a specified id
	 *
	 * @param  string   Get all statements up to this id
	 * @return array 	The array of Statement Objects
	 */
	public function getStatementsUpTo($id){
		$filenames = array();
		$statements = array();

		$directory = getcwd() . '/' . $this->defaultDataDirectoryName;
		$filenames = array_diff(scandir($directory, 1), array('..', '.'));

		foreach($filenames as $filename) {

			if($filename == $id) {
				break;
			}
			$statements[] = $this->getStatement($filename);
		}
		
		return $statements;
	}

	/**
	 * Returns all statements as a json string.
	 * The json starts with a named array 'statements' which contains the statements.
	 * The method only returns the latest statements up to a specified id if
	 * the id is passed as a parameter
	 *
	 * @param  string   Get all statements up to the id; null return all Statements.
	 * @return string 	The json string
	 */
	public function getStatementsAsJson($id = null){

		if($id == null) {
			$statements['statements'] = $this->getAllStatements();
		} else {
			$statements['statements'] = $this->getStatementsUpTo($id);
		}
		return json_encode($statements);
	}

	/**
	 * Reads Statement data from POST and invokes the necessary
	 * methods to save the statement.
	 *
	 * @return string		The id of the new statement
	 */
	public function processFormData(){
		
		$statementId = $this->getUniqueIdentifier();

		$base64ImageString = isset($_POST['image']) ? $_POST['image'] : '';
		
		$title = isset($_POST['title']) ? $_POST['title'] : 'Kein Statement';
		$latitude = isset($_POST['latitude']) ? floatval($_POST['latitude']) : 51.1642292;
		$longitude = isset($_POST['longitude']) ? floatval($_POST['longitude']) : 10.4541194;
		$category = isset($_POST['category']) ? $_POST['category'] : 'allgemein';
		$location = isset($_POST['location']) ? $_POST['location'] : '';
		$hasImage = (isset($_POST['image']) && !empty($_POST['image'])) ? true : false;

		$statement = new Statement($statementId, $title, $latitude, $longitude, $category, $location, $hasImage);
		$this->addStatement($statement, $base64ImageString);
		
		return $statementId;
	}

	/**
	 * Adds a statement to the data directory.
	 *
	 * @param 	string 	$statement 		The Statement object
	 * @param 	string 	$base64image 	The image as a base64 encoded string
	 */
	public function addStatement($statement, $base64Image){
		
		$statementId = $statement->getID();

		// Create image directory
		$this->createImageDirectory($statementId);

		// Decode and save image
		if ($base64Image) {
			$this->decodeImageAndSaveToFile($base64Image, $statementId);
		}

		// Construct and save statement
		$this->saveStatementToFile($statement, $statementId);
	}

	/**
	 * Reads a statement from a data file and returns a Statement object.
	 *
	 * @param 	string 	$statementId 	The directory in which the data file resides
	 * @return 	Statement or boolean 	The statement, false if not found
	 */
	public function getStatement($statementId){
		
		$dataFilePath = $this->getProjectDir() . $this->defaultDataDirectoryPath . '/' . $statementId . '/' . $this->defaultDataFileName;
		if( file_exists($dataFilePath)){
			return unserialize(file_get_contents($dataFilePath));
		} else {
			return false;
		}
	}

	/**
	 * Deletes a statement (the whole statement directory containing 
	 * the serialized object and the image).
	 *
	 * @param 	string 	$statementId 	The directory in which the files resides
	 */
	public function deleteStatement($statementId){

		$directory = getcwd() . '/' . $this->defaultDataDirectoryName . '/' . $statementId . '/';

		unlink($directory . $this->defaultDataFileName);
		if( file_exists($directory . $this->defaultImageName) ) {
			unlink($directory . $this->defaultImageName);
		}
		rmdir($directory);
	}

	/**
	 * Creates a directory inside the data directory with $directoryName as the name.
	 *
	 * @param 	string 	$directoryName 	The name of the directory
	 */
	private function createImageDirectory($directoryName){
		$imageDirectoryPath = getcwd() . '/' . $this->defaultDataDirectoryName . '/' . $directoryName . '/';
		mkdir($imageDirectoryPath);
	}

	/**
	 * Decodes a base64 encoded jpeg into a php image and saves it 
	 * inside the data directory in a directory with the name $statementId.
	 * The base64 encoded image is expected as a data url like so:
	 * data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2...
	 *
	 * @param 	string 	$base64ImageString 	The base64 encoded image
	 * @param 	string 	$statementId 		The directory name
	 */
	private function decodeImageAndSaveToFile($base64ImageString, $statementId){

		$base64Image = str_replace('data:image/jpeg;base64,', '', $base64ImageString);

		$decodedData = base64_decode($base64Image);
		$image = imagecreatefromstring($decodedData);
		$imageDirectoryPath = getcwd() . '/' . $this->defaultDataDirectoryName . '/' . $statementId . '/';

		imagejpeg($image, $imageDirectoryPath . $this->defaultImageName, 80);
		chmod($imageDirectoryPath . $this->defaultImageName, 0777);
	}

	/**
	 * Serializes a Statement object to a file.
	 * 
	 * @param 	Statement 	$statement 	The statement
	 * 
	 */
	private function saveStatementToFile($statement, $statementId){
		$filename = getcwd() . '/' . $this->defaultDataDirectoryName . '/' . $statementId . '/' . $this->defaultDataFileName;
		file_put_contents($filename, serialize($statement));
		chmod($filename, 0777);
	}

	/**
	 * Returns a unique identifier.
	 * The identifier is a string from the date call date('Y-m-d-H-i-s').
	 *
	 * @return 	string 	The identifier
	 */
	private function getUniqueIdentifier() {

		$statementId = date('Y-m-d-H-i-s') . '-' . substr(md5(time()), 0, 5);
		return $statementId;
	}
	
	/**
	 * Get the email text
	 */
	
	private function getEmailText() {
		
		return file_get_contents('../email.txt');
	}
	
	/**
	 * Sends the Statement via email
	 */
	
	public function sendMail($email, $link) {
		
		$text = str_replace('[link]', '<a href="' . $link . '">' . $link . '</a>', nl2br($this->getEmailText()));
		$headers = 'From: ' . $this->emailFrom . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		mail($email, $this->emailSubject, $text, $headers);
	}
	
	private function getProjectDir() {
	    global $argv;
	    $dir = dirname(getcwd() . '/' . $argv[0]);
	    $curDir = getcwd();
	    chdir($dir);
	    $dir = getcwd();
	    chdir($curDir);
	    return $dir;
	}
}

?>