angular.module('UserInterface1', ['google-maps', 'ngRoute']).controller('PhotoBooth', ['$scope', '$route', '$routeParams', '$location', '$http', function($scope, $route, $routeParams, $location, $http) {

	//Ng-Route
	$scope.$route = $route;
	$scope.$location = $location;
	$scope.$routeParams = $routeParams;

	/**
	 * Statement & page
	 */
	$scope.page = 1;
	$scope.progress = 10;
	$scope.notFound = false;
	$scope.submitting = false;
	
	if (window.location.href.replace('/id/', '') == window.location.href) {
		$scope.statement = {
			'image' : '',
			'title' : '',
			'latitude' : '',
			'longitude' : '',
			'category': '',
			'location' : '',
			'hasImage' : false
		};
	} else {
	
		$scope.page = 3;
		
		var data = {
			statementId: window.location.href.split('/id/')[1],
			action: 'get-statement'
		};

		$http({
			method: 'POST',
			url: 'backend/index.php',
			data: data,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
    		var str = [];
    		for(var p in obj)
    		str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    		return str.join("&");
    		}
		}).success(function(returnData, status, headers, config) {
			
			if (returnData == 'Not found') {
				
				$scope.notFound = true;
				
			} else {
			
				$scope.statement = {
					coords: {
						latitude: returnData.latitude,
						longitude: returnData.longitude
					},
					options: {
						'pixelOffset': new google.maps.Size(0, -36)
					},
					title: returnData.title,
					hasImage: returnData.hasImage,
					location: returnData.location,
					category: returnData.category
				};
				
				if (returnData.hasImage) {
					$scope.statement.image = 'backend/data/' + returnData.id + '/image.jpg';
				}
			}
			
			
			
		}).
		error(function(data, status, headers, config) {
			console.error("The statement could not be load.");
		});
	}
	
	/**
	 * Webcam
	 *
	 * Options and initial parameters for the Webcam.
	 * The Webcam is not a scope object because it is an external script.
	 */
	Webcam.set({
		image_format: 'jpeg',
		jpeg_quality: 90,
		force_flash: false
	});
	Webcam.attach('#webcam');

	/**
	 * Map
	 *
	 * Options and initial parameters for the google map.
	 */
	$scope.map = {
		center: {
			latitude: 51.3,
			longitude: 10.5
		},
		zoom: 6,
		options: {
			maxZoom: 9,
			minZoom: 6,
			streetViewControl: false
		}
	};

	/**
	 * Marker
	 * 
	 * Should be dragged by the user to a location.
	 * The dragend event is defined later in this file.
	 */
	$scope.marker = {
		id: 1,
		icon: "http://mt.googleapis.com/vt/icon/name=icons/spotlight/spotlight-poi.png&scale=1.5",
		coords: {
			latitude: 51.2681,
			longitude: 10.3657
		},
		options: {
			draggable: true,
			animation: 1
		},
		events: {
			dragend: ''
		}
	};

	$scope.showStatementForm = false;

	$scope.categories = [];

	/**
	 * Make a photo and add it to the statement
	 */
	$scope.takeSnapshot = function() {
		$scope.statement.image = Webcam.snap();
		$scope.progress = 25;
	}
	
	$scope.goToPage = function(page) {
		$scope.page = page;
		
		if (page == 4) {
			$scope.getAllStatements();
		}
	}

	/**
	 * Submit the statement data
	 */
	$scope.submitStatement = function() {
		
		$scope.submitting = true;
		
		var data = $scope.statement;
		data['action'] = 'add-statement';

		$http({
			method: 'POST',
			url: 'backend/index.php',
			data: data,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
        		var str = [];
        		for(var p in obj)
        		str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        		return str.join("&");
    		}
		}).success(function(returnData, status, headers, config) {
		
			//When the app is already reset, don't proceed
			if ($scope.submitting) {
				$scope.statement.coords = {'latitude': data.latitude, 'longitude': data.longitude };
				$scope.statement.options = {'pixelOffset': new google.maps.Size(0, -36) };
				
				$location.path('/id/' + returnData);
				$scope.progress = 100;
				$scope.page = 3;
			}
			
		}).
		error(function(data, status, headers, config) {
			console.error("The statement could not be sent.");
		});
	};

	$scope.getCategories = function() {

		var data = {
			action: 'get-categories'
		};

		$http({
			method: 'POST',
			url: 'backend/index.php',
			data: data,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
	    		var str = [];
	    		for(var p in obj)
	    		str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	    		return str.join("&");
    		}
		}).success(function(returnData, status, headers, config) {
			
			$scope.categories = returnData;		
		}).
		error(function(data, status, headers, config) {
			console.error("The categories could not be loaded.");
		});
	};
	$scope.getCategories();
	
	$scope.sendMail = function() {
	
		var data = {
			action: 'send-email',
			link: window.location.href,
			email: $scope.email
		};

		$http({
			method: 'POST',
			url: 'backend/index.php',
			data: data,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
        		var str = [];
        		for(var p in obj)
        		str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        		return str.join("&");
    		}
		}).success(function(returnData, status, headers, config) {
		
			if (returnData == 'OK') {
			
				$scope.email = '';
				$scope.mailAlert = 'success';
				
			} else {
				$scope.mailAlert = 'error';
			}

		}).
		error(function(data, status, headers, config) {
			$scope.mailAlert = 'error';
		});
	}
	
	/**
	 * Reset App (start again)
	 */
	 
	 $scope.reset = function() {
	 
	 	$location.path('');
	 	$routeParams.statementId = null;
	 
	 	$scope.page = 1;
	 	$scope.progress = 10;
	 	$scope.showStatementForm = false;
	 	$scope.notFound = false;
	 	$scope.submitting = false;
	 	$scope.mailAlert = '';
	 	$scope.email = '';
	 	
	 	$scope.statement = {
	 		'image' : '',
	 		'title' : '',
	 		'latitude' : '',
	 		'longitude' : '',
	 		'location' : '',
	 		'hasImage': false
	 	};
	 	
	 	$scope.map.center = {
 			latitude: 51.3,
 			longitude: 10.5
	 	}
	 	
	 	$scope.map.zoom = 6;
	 	
		$scope.marker.coords = {
			latitude: 51.2681,
			longitude: 10.3657
		};
		
		$scope.marker.animation = 1;
	 };

	/**
	 * Add dragstart to the marker to stop the
	 * bounce animation.
	 */
	$scope.marker.events.dragstart = function (marker, eventName, args) {
		marker.setAnimation(null);
	};

	/**
	 * Add dragend event to the marker
	 * to update the coordinates and the loaction.
	 */
	$scope.marker.events.dragend = function (marker, eventName, args) {
		
		$scope.showStatementForm = true;
		$scope.progress = 70;
			
		geocoder = new google.maps.Geocoder();
		geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
			
			$scope.statement.location = $scope.getAreaFromResults(results);
			$scope.$apply();
		});
	
		//Manually update $scope
		$scope.marker.coords = {latitude: marker.getPosition().lat(), longitude: marker.getPosition().lng()};

		$scope.statement.latitude = marker.getPosition().lat();
		$scope.statement.longitude = marker.getPosition().lng();
	};
	
	/**
	 * Function to get a location (string) from coordinates
	 */
	$scope.getAreaFromResults = function(results) {
		
		var location = '';
		
		//Suche nach Region (z.B. "Oberfranken")
		angular.forEach(results, function(result, resNr) {
			angular.forEach(result.address_components, function(component, compNr) {
				if (component.types.indexOf('administrative_area_level_2') != -1) {
					location = component.long_name;
				}
			});
		});
		
		//Falls nicht gefunden, suche nach Stadt (z.B. Karlsruhe)
		if (location == '') {
			angular.forEach(results, function(result, resNr) {
				angular.forEach(result.address_components, function(component, compNr) {
					if (component.types.indexOf('administrative_area_level_3') != -1) {
						location = component.long_name;
					}
				});
			});
		}
		
		//Falls nicht gefunden, suche nach Stadt (z.B. Karlsruhe)
		if (location == '') {
			angular.forEach(results, function(result, resNr) {
				angular.forEach(result.address_components, function(component, compNr) {
					if (component.types.indexOf('administrative_area_level_1') != -1) {
						location = component.long_name;
					}
				});
			});
		}
		
		return location;
	};
	
	$scope.getAllStatements = function() {

		var startAt = '';
		var data = {
			'action' : 'get-statements',
			'startAt': startAt
		}

		$http({
			method: 'POST', 
			url: 'backend/index.php', 
			data: data, 
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}, 
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
		}}).
		success(function(data, status, headers, config) {
			$scope.allStatements = data.statements;
			
			angular.forEach($scope.allStatements, function(statement, nr) {
				$scope.allStatements[nr].coords = {
					latitude: statement.latitude,
					longitude: statement.longitude
				};
				$scope.allStatements[nr].show = null;
			});
			
		}).
		error(function(data, status, headers, config) {
			console.log(data);
		});
	};
	
	$scope.allStatementsOptions = {
		'pixelOffset': new google.maps.Size(0, -36)
	};
	

}]).config(function($routeProvider, $locationProvider) {

	//Route the StatementId
  $routeProvider
  .when('/id/:statementId', {
    controller: 'PhotoBooth'
  });
});

Webcam.on( 'live', function() {
	$('#wrapper-snap-buttons').addClass('webcamReady');
});